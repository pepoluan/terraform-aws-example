variable "product" {
  type = string
  default = "Greatest Product on Earth"
}

variable "environment" {
  type = string
  default = "Production"
}

variable "subproduct_title" {
  type = string
  default = "GreatestProduct01"
}

variable "subproduct" {
  type = string
  default = "gp01"
}

variable "keypair" {
  type        = string
  description = "The NAME of the SSH Key Pair for the `ubuntu` account in EC2's 'Key Pairs' section"
  default     = "kp-gp01"
}

variable "usarr_pubkey" {
  type        = string
  description = "SSH Pubkey for the 'usarr' account"
  default     = "ssh-ed25519 AAAAC3..."
}

variable "publicized_http_ports" {
  type = list(string)
  default = [ "80", "8000", "8080" ]
}

variable "custom_ssh_ports" {
  type = list(string)
  description = "EXCLUDING port 22"
  default = [ "2222", "65522" ]
}

variable "bastion" {
  default = {
    ami       = "gold-bastion-20200424a"
    subnet    = "public-a"
    type      = "t2.small"
    ip        = "10.11.20.70"
    whitelist = {
      "Pandu Home" = [ "118.137.246.181/32" ]
      "The Office" = [ "56.99.123.234/32" ]
    }
  }
}

variable "ip_addr" {
  type = map
  default = {
      web     = "10.11.21.11"
      app     = "10.11.21.12"
  }
}

variable "servers_ami_name" {
  type        = string
  description = "The AMI name for servers"
  default     = "gold-servers-20200424a"
}

locals {
  subnets_others = {
    # NOTE: Last letter denotes the zone!

    # Public Subnets
    public-a = {
      cidr = "10.11.20.64/26"
      name = "${local.SUBPROD_UPPER}-Prod-Public-A"
      public = true
    }
    public-b = {
      cidr = "10.11.21.64/26"
      name = "${local.SUBPROD_UPPER}-Prod-Public-B"
      public = true
    }

    # Private Subnets
    # WARNING: ensure each of these has "public = false" !!!
    private-a = {
      cidr = "10.11.20.0/26"
      name = "${local.SUBPROD_UPPER}-Prod-Private-A"
      public = false
    }
    private-b = {
      cidr = "10.11.21.0/26"
      name = "${local.SUBPROD_UPPER}-Prod-Private-B"
      public = false
    }
  }

  subnets_db = {
    # NOTE: Last letter denotes the zone!

    # Private Subnets
    # WARNING: ensure each of these has "public = false" !!!
    db-a = {
      cidr = "10.11.20.128/26"
      name = "${local.SUBPROD_UPPER}-Prod-DB-A"
      public = false
    }
    db-b = {
      cidr = "10.11.21.128/26"
      name = "${local.SUBPROD_UPPER}-Prod-DB-B"
      public = false
    }
  }

  loadbalancer = {
    subnets = [ "public-a", "public-b" ]
  }

  servers = {
    subnet = "public-b"
  }

  # Derivatives and Constants -- DO NOT EDIT BELOW THIS LINE

  subprod_lower = lower(var.subproduct)
  SUBPROD_UPPER = upper(var.subproduct)  

  common_tags = {
    Environment = var.environment
    Product     = var.product
    SubProduct  = "${local.SUBPROD_UPPER}"
  }

  ami_owner_id = "1234567890"
  aws_region   = "ap-southeast-1"
}
