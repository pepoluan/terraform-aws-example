## Guidelines

* You have to prepare the AMI yourself.

* Nearly all settings are in the root of this repo, but there are some in individual .tf files under
  `databases/` and `others/`.

* The `databases/` recipe needs the `local.credentials` variable. This should be provided by a .tf file
  whose name ends with `.gitignore.tf` (to prevent accidental commit into repo)

## Deploying

> **NOTE:** This recipe _as is_ will not be executable; running `terraform apply` _will_ result in an error.

To run the recipe -- after customizing and setting the parameters, of course -- you do it in two steps:

### Step 1: Deploy the database

```bash
cd database
terraform init
terraform validate
terraform plan
terraform apply
cd ..
```

### Step 2: Deploy the others

```bash
cd others
terraform init
terraform validate
terraform plan
terraform apply
cd ..
```

## Destroying

To destroy, you do the reverse of the Deploying steps.

However, this Terraform "recipe" has been designed so you can destroy the servers (and loadbalancer) while keeping the RDS intact, so _in theory_ you should be able to redeploye the latest-and-greatest version of your app by destroying the existing servers and redeploying.

In fact, by splitting the recipe like this, it is possible to simply create a new AMI (with a different name) for the servers, change the AMI used by the servers (in `variables-common.tf`) and just do `terraform apply` in `others/`; Terraform will see that the AMI is different and perform the destroy/create automatically.


## LICENSE

All code in this repo is released under the "Unlicense" License.

See the `LICENSE` file in the root of this repo for the complete legalese text.

## Contributing

Please see the `CONTRIBUTING` file for contribution guidelines.
