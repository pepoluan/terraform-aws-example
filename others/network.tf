
module "network" {
  source = "../_common_modules_/network"

  # Specific Config for "others"
  subnets = local.subnets_others

  # Common Config
  vpc_id  = local.prod.vpc.id
  region  = local.aws_region
  tag_environment = var.environment
  tag_product     = var.product
  tag_subproduct  = local.SUBPROD_UPPER
  routing_tables  = {
    public_id  = local.prod.routing-public.id
    private_id = local.prod.routing-private.id
  }
}
