
data "aws_ami" "gold-bastion" {
  most_recent = true

  filter {
    name   = "name"
    values = [ var.bastion.ami ]
  }

  owners = [ local.ami_owner_id ]
}

resource "aws_security_group" "prod-bastion-external" {
    name        = "${local.subprod_lower}-prod-bastion-external"
    description = "${var.subproduct_title} Security Group for Bastion -- MANAGED BY TERRAFORM"
    vpc_id      = local.prod.vpc.id

    tags = merge(local.common_tags, {
      Name = "SG-${local.SUBPROD_UPPER}-PROD-BASTION-EXT"
    })
}

resource "aws_security_group_rule" "prod-bastion-external-ssh" {
  for_each = toset(var.custom_ssh_ports)

  security_group_id = aws_security_group.prod-bastion-external.id
  description   = "Incoming SSH Custom"
  type          = "ingress"
  cidr_blocks   = [ "0.0.0.0/0" ]
  protocol      = "tcp"
  from_port     = each.key
  to_port       = each.key
}

resource "aws_security_group_rule" "prod-bastion-external-whitelist" {
  for_each = var.bastion.whitelist

  security_group_id = aws_security_group.prod-bastion-external.id
  description   = "Incoming ALL from ${each.key}"
  type          = "ingress"
  cidr_blocks   = each.value
  protocol      = "-1"
  from_port     = 0
  to_port       = 0
}

resource "aws_security_group_rule" "prod-bastion-external-egress" {
  security_group_id = aws_security_group.prod-bastion-external.id
  description = "Outgoing ALL"
  type        = "egress"
  cidr_blocks = [ "0.0.0.0/0" ]
  from_port   = 0
  to_port     = 0
  protocol    = -1
}

resource "aws_instance" "prod-bastion" {
  ami           = data.aws_ami.gold-bastion.id
  instance_type = var.bastion.type
  subnet_id     = module.network.subnets[var.bastion.subnet].id
  private_ip    = var.bastion.ip
  
  key_name = var.keypair
  vpc_security_group_ids = [
    aws_security_group.prod-bastion-external.id
  ]

  # Used by cloud-init to perform at-launch things
  user_data = <<-_EOF
    #!/bin/bash
    sudo hostnamectl set-hostname ${local.subprod_lower}-prod-bastion
    printf "\n%s\n" "${var.usarr_pubkey}" >> /home/usarr/.ssh/authorized_keys
    netfilter-persistent save
  _EOF

  tags = merge(local.common_tags, {
    Name = "${local.SUBPROD_UPPER}-Prod-Bastion"
  })
}

resource "aws_eip" "prod-bastion" {
  instance = aws_instance.prod-bastion.id
  vpc      = true

  tags = merge(local.common_tags, {
    Name = "${local.SUBPROD_UPPER}-Prod-Bastion"
  })
}
