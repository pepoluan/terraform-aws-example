locals {
  db_outputs = data.terraform_remote_state.db.outputs
  db_port    = db_outputs.instance.port
  db_secgrp  = db_outputs.secgrp

  source_secgrps = {
    "Prod EC2"     = aws_security_group.prod-ec2.id
    "Prod Bastion" = aws_security_group.prod-bastion-external.id
  }
}

resource "aws_security_group_rule" "prod-db" {
  for_each = local.source_secgrps

  security_group_id = local.db_secgrp.id
  description = "Incoming from ${local.SUBPROD_UPPER}-${each.key}"
  type        = "ingress"
  protocol    = "tcp"
  from_port   = local.db_port
  to_port     = local.db_port
  source_security_group_id = each.value
}
