
resource "aws_security_group" "prod-lb" {
    name        = "${local.subprod_lower}-prod-lb"
    description = "${var.subproduct_title} Security Group for LoadBalancers -- MANAGED BY TERRAFORM"
    vpc_id      = local.prod.vpc.id

    tags = merge(local.common_tags, {
        Name = "SG-${local.SUBPROD_UPPER}-PROD-LB"
    })
}

resource "aws_security_group_rule" "prod-lb-https" {
  security_group_id = aws_security_group.prod-lb.id
  description   = "Incoming HTTPS"
  type          = "ingress"
  cidr_blocks   = [ "0.0.0.0/0" ]
  protocol      = "tcp"
  from_port     = 443
  to_port       = 443
}

resource "aws_lb" "prod-lb" {
    name               = "lb-${local.subprod_lower}-prod"
    internal           = false
    load_balancer_type = "application"
    security_groups    = [
      aws_security_group.prod-lb.id,
      aws_security_group.prod-web.id
    ]
    subnets            = [
      for s in local.loadbalancer.subnets: module.network.subnets[s].id
    ]

    enable_deletion_protection = false

    tags = merge(local.common_tags, {
        Name = "${local.SUBPROD_UPPER}-PROD-ALB"
    })
}

resource "aws_lb_listener" "prod-listener" {
    for_each = toset(var.publicized_http_ports)

    load_balancer_arn = aws_lb.prod-lb.arn
    port = each.key
    protocol = "HTTP"
    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.prod-targroup[each.key].arn
    }
}

resource "aws_lb_target_group" "prod-targroup" {
  for_each = toset(var.publicized_http_ports)

  name     = "${local.subprod_lower}-prod-targroup-${each.key}"
  port     = each.key
  protocol = "HTTP"
  vpc_id   = local.prod.vpc.id

  tags = merge(local.common_tags, {
    Name = "${local.SUBPROD_UPPER}-PROD-TARGRP-${each.key}"
  })
}

resource "aws_lb_target_group_attachment" "prod-target" {
  for_each = toset(var.publicized_http_ports)

  target_group_arn = aws_lb_target_group.prod-targroup[each.key].arn
  target_id        = aws_instance.web.id
  port             = each.key
}
