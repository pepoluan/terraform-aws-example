
data "aws_ami" "cw-gold-cp" {
  most_recent = true
  filter {
    name   = "name"
    values = [ var.servers_ami_name ]
  }
  owners = [ local.ami_owner_id ]
}

locals {
  servers_subnet_id = module.network.subnets[local.servers.subnet].id
}

##### WEB SERVER

resource "aws_instance" "web" {
  ami           = data.aws_ami.cw-gold-cp.id
  instance_type = "m5a.large"
  subnet_id     = local.servers_subnet_id
  private_ip    = var.ip_addr.web
  
  key_name = var.keypair
  vpc_security_group_ids = [
    aws_security_group.prod-ec2.id,
    aws_security_group.prod-web.id
  ]

  # Used by cloud-init to perform at-launch things
  user_data = <<-_EOF
    #!/bin/bash
    sudo hostnamectl set-hostname ${local.subprod_lower}-prod-web
    printf "\n%s\n" "${var.usarr_pubkey}" >> /home/usarr/.ssh/authorized_keys
  _EOF

  tags = merge(local.common_tags, {
    Name = "${local.SUBPROD_UPPER}-Prod-Srv-Web"
  })
}

##### APP SERVER

resource "aws_instance" "app" {
  ami           = data.aws_ami.cw-gold-cp.id
  instance_type = "c5.xlarge"
  subnet_id     = local.servers_subnet_id
  private_ip    = var.ip_addr.app

  key_name = var.keypair
  vpc_security_group_ids = [
    aws_security_group.prod-ec2.id
  ]

  # Used by cloud-init to perform at-launch things
  user_data = <<-_EOF
    #!/bin/bash
    sudo hostnamectl set-hostname ${local.subprod_lower}-prod-app
    printf "\n%s\n" "${var.usarr_pubkey}" >> /home/usarr/.ssh/authorized_keys
  _EOF

  tags = merge(local.common_tags, {
    Name = "${local.SUBPROD_UPPER}-Prod-Srv-App"
  })
}
