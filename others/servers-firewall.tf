
resource "aws_security_group" "prod-ec2" {
  name        = "${local.subprod_lower}-prod-ec2-internal"
  description = "${var.subproduct_title} SecGrp for EC2 Internal Traffic -- MANAGED BY TERRAFORM"
  vpc_id      = local.prod.vpc.id

  tags = merge(local.common_tags, {
    Name = "SG-${local.SUBPROD_UPPER}-PROD-EC2-INTERNAL"
  })
}

resource "aws_security_group_rule" "prod-ec2-egress" {
  security_group_id = aws_security_group.prod-ec2.id
  description = "Outgoing ALL"
  type        = "egress"
  protocol    = "-1"  # All protocols
  # If protocol is "-1", from_port and to_port must both be zero
  # Ref: https://www.terraform.io/docs/providers/aws/r/security_group.html#from_port
  from_port   = 0
  to_port     = 0
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_security_group_rule" "prod-ec2-ingress-internal" {
  security_group_id = aws_security_group.prod-ec2.id
  description   = "Incoming ALL"
  type          = "ingress"
  cidr_blocks   = [ for k, v in local.subnets_others : v.cidr ]
  protocol      = "-1"
  # If protocol is "-1", from_port and to_port must both be zero
  # Ref: https://www.terraform.io/docs/providers/aws/r/security_group.html#from_port
  from_port     = 0
  to_port       = 0
}

resource "aws_security_group" "prod-web" {
  name        = "${local.subprod_lower}-prod-web"
  description = "${var.subproduct_title} SecGrp for HTTP from LB -- MANAGED BY TERRAFORM"
  vpc_id      = local.prod.vpc.id

  tags = merge(local.common_tags, {
    Name = "SG-${local.SUBPROD_UPPER}-PROD-WEB"
  })
}

resource "aws_security_group_rule" "prod-web-egress" {
  security_group_id = aws_security_group.prod-web.id
  description = "Outgoing ALL"
  type        = "egress"
  protocol    = "-1"  # All protocols
  # If protocol is "-1", from_port and to_port must both be zero
  # Ref: https://www.terraform.io/docs/providers/aws/r/security_group.html#from_port
  from_port   = 0
  to_port     = 0
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_security_group_rule" "prod-web-ingress" {
  for_each = toset(var.publicized_http_ports)

  security_group_id = aws_security_group.prod-web.id

  description = "Incoming HTTP"
  type        = "ingress"
  protocol    = "tcp"
  from_port   = each.key
  to_port     = each.key
  cidr_blocks = [ "0.0.0.0/0" ]
}
