provider "aws" {
  // Access Key ID and Access Secret provided via env
  version = "~> 2.0"
  region = "ap-southeast-1"
}

data "terraform_remote_state" "prod" {
  backend = "s3"
  config = {
    bucket = "some-bucket"
    #### WARNING: CHECK THE .tfstate FILE'S NAME
    key = "tf-states/production.tfstate"
    region = "ap-southeast-1"
  }
}

# Store in a local variable so we don't have to always type the longform

locals {
  prod = data.terraform_remote_state.prod.outputs
}
