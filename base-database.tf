
terraform {
  backend "s3" {
    bucket = "some-bucket"
    #### WARNING: CHECK THE .tfstate FILE'S NAME
    # Don't forget to sync with `base-others.tf`
    key = "tf-states/gp01-prod_database.tfstate"
    dynamodb_table = "tf-state-locking"
  }
}
