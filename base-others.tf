
terraform {
  backend "s3" {
    bucket = "some-bucket"
    #### WARNING: CHECK THE .tfstate FILE'S NAME
    key = "tf-states/gp01-prod.tfstate"
    dynamodb_table = "tf-state-locking"
  }
}

data "terraform_remote_state" "db" {
  backend = "s3"
  config = {
    bucket = "some-bucket"
    #### WARNING: CHECK THE .tfstate FILE'S NAME
    # Please check `base-database.tf`
    key = "tf-states/gp01-prod_database.tfstate"
    region = "ap-southeast-1"
  }
}
