variable "subnets" {
  type = map    
  description = <<EOD
    This is a map of subnets to create.

    The keys are the subnets' "internal identification", must NOT contain spaces.

    The values are maps containing the following fields:
      - cidr = a string specifying the CIDR block
      - name = a string that will be set as the "Name" tag of the subnet
      - public = a bool that specifies if the subnet is public or not
  EOD
}

variable "vpc_id" {
    type = string
    description = "The ID of the VPC where the subnets will be created"
}

variable "region" {
    type = string
    description = "The region in which the subnets will be created"
}

variable "tag_environment" {
    type = string
    description = "The contents of the 'Environment' tag, e.g., 'Production' or 'Staging' or 'Dev' etc"
}

variable "tag_product" {
    type = string
    description = "The contents of the 'Product' tag, for informational/filtering purposes"
}

variable "tag_subproduct" {
    type = string
    description = "The contents of the 'SubProduct' tag, for informational/filtering purposes"
    default = null
}

resource "aws_subnet" "subnet" {
  for_each = var.subnets

  vpc_id   = var.vpc_id

  availability_zone       = "${var.region}${substr(each.key, -1, 1)}"
  cidr_block              = each.value.cidr
  map_public_ip_on_launch = each.value.public

  tags = {
    Name = each.value.name
    Environment = var.tag_environment
    Product = var.tag_product
    SubProduct = var.tag_subproduct
  }
}

output "subnets" {
  value = aws_subnet.subnet
}
