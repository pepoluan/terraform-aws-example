variable "routing_tables" {
  type = object({
    public_id = string,
    private_id = string
  })
  description = <<EOD
    This is an object (map) containing two attributes:
      - public_id  = The ID of the Public Routing Table
      - private_id = The ID of the Private Routing Table

    You have to pre-create the pair of routing tables before you can use this module.
  EOD
}

resource "aws_route_table_association" "rtbassoc" {
  for_each  = var.subnets

  subnet_id      = aws_subnet.subnet[each.key].id
  route_table_id = each.value.public ? var.routing_tables.public_id : var.routing_tables.private_id
}
