
module "network" {
  source = "../_common_modules_/network"

  # Specific Config for "database"
  subnets = local.subnets_db

  # Common Config
  vpc_id  = local.prod.vpc.id
  region  = local.aws_region
  tag_environment = var.environment
  tag_product     = var.product
  tag_subproduct  = local.SUBPROD_UPPER
  routing_tables  = {
    public_id  = local.prod.routing-public.id
    private_id = local.prod.routing-private.id
  }
}

resource "aws_db_subnet_group" "prod-db" {
    name       = "snetg-${local.subprod_lower}-prod-db"
    subnet_ids = [
      # aws_subnet.subnet["db-a"].id,
      # aws_subnet.subnet["db-b"].id
      module.network.subnets["db-a"].id,
      module.network.subnets["db-b"].id
    ]
    tags = merge(local.common_tags, {
      Name = "${local.SUBPROD_UPPER}-Prod-SNETG-DB"
    })
}
