
resource "aws_db_parameter_group" "prod-db" {
  name        = "dbpg-${local.subprod_lower}-prod-my-80"
  family      = "mysql8.0"
  description = "${var.subproduct_title} MySQL 8.0 Parameter Group -- MANAGED BY TERRAFORM"

  parameter {
    name  = "time_zone"
    value = "Asia/Bangkok"
  }
}

resource "aws_db_option_group" "prod-db" {
  name                     = "dbopt-${local.subprod_lower}-prod-my-80"
  option_group_description = "${var.subproduct_title} MySQL 8.0 Option Group -- MANAGED BY TERRAFORM"
  engine_name              = "mysql"
  major_engine_version     = "8.0"
}

resource "aws_db_instance" "prod-db" {
    depends_on = [ aws_db_parameter_group.prod-db, aws_db_option_group.prod-db ]
    identifier            = "db-${local.subprod_lower}-prod-my"
    
    multi_az               = true
    db_subnet_group_name   = aws_db_subnet_group.prod-db.name
    vpc_security_group_ids = [ aws_security_group.prod-db.id ]

    instance_class        = "db.m5.xlarge"
    allocated_storage     = 100
    max_allocated_storage = 500
    storage_type          = "gp2"

    engine               = "mysql"
    engine_version       = "8.0.17"
    parameter_group_name = aws_db_parameter_group.prod-db.name
    option_group_name    = aws_db_option_group.prod-db.name

    monitoring_interval             = 30
    monitoring_role_arn             = "arn:aws:iam::228525134900:role/rds-monitoring-role"
    performance_insights_enabled    = true
    enabled_cloudwatch_logs_exports = [ "error", "slowquery" ]

    backup_retention_period   = 7
    backup_window             = "19:00-19:30"  # UTC
    copy_tags_to_snapshot     = true
    skip_final_snapshot       = false
    final_snapshot_identifier = "${local.SUBPROD_UPPER}-Prod-DB-My-FinalSnaphot"

    username = local.credentials.db_my.username
    password = local.credentials.db_my.password

    tags = merge(local.common_tags, {
      Name = "${local.SUBPROD_UPPER}-Prod-DB-My"
    })
}

output "instance" {
  value = aws_db_instance.prod-db
}
