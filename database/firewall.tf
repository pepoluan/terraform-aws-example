
# ATTENTION:
# We do NOT specify any rule here, to allow the "others/" part of the recipe to
# specify actual rules to inject.

resource "aws_security_group" "prod-db" {
    name        = "${local.subprod_lower}-prod-db"
    description = "${var.subproduct_title} Security Group for DB Internal Traffic -- MANAGED BY TERRAFORM"
    vpc_id      = local.prod.vpc.id

    tags = merge(local.common_tags, {
        Name = "SG-${local.SUBPROD_UPPER}-PROD-DB"
    })
}

output "secgrp" {
  value = aws_security_group.prod-db
}
